export interface Recipe {
    id: number;
    title: string;
    image: string;
    time: number;
    description: string;
    vegan: boolean;
}

const getRecipes = async (): Promise<Recipe[]> => {
    const response = await fetch("http://localhost:4200/recipes");
    return response.json();
}

export default getRecipes;