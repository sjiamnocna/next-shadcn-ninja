import { SunIcon } from "lucide-react";
import { useTheme } from "next-themes";
import { BiMoon } from "react-icons/bi";
import { FaCloudMoon, FaMoon, FaRegMoon } from "react-icons/fa";
import { LiaLightbulb } from "react-icons/lia";

const ThemeSwitcher = () => {
    const { setTheme, theme } = useTheme();

    const isDark:boolean = theme === 'dark';

    return (
        <button onClick={() => setTheme(isDark ? 'light' : 'dark')}>
            {isDark ? <LiaLightbulb size={24} /> : <FaRegMoon />}
        </button>
    );
}

export default ThemeSwitcher;