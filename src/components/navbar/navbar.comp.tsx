"use client"

import * as React from "react"
import Link from "next/link"
import { PiHamburgerDuotone } from "react-icons/pi"
import { TbCurrencyKroneCzech } from "react-icons/tb"
import { FlagIcon } from "lucide-react"
import { FaFlag, FaFlagCheckered } from "react-icons/fa"
import ThemeSwitcher from "./theme-switcher.comp"

type NavItem = {
  title: string;
  path: string;
}

type NavItemWithChildren = NavItem & {
  children?: NavItem[];
}

type NavbarProps = {
  items: NavItemWithChildren[];
}

export default function Navbar({ items }: NavbarProps) {
  const [opened, setState] = React.useState(false);

  return (
    <nav className="w-full border-b-2">
      <div className="items-center px-3 max-w-screen-xl mx-auto md:flex md:px-8">
        <div className="flex items-center justify-between md:block">
          <Link href="/">
            <h1 className="text-5xl font-bold text-purple-600">Ninja</h1>
          </Link>
          <div className="md:hidden">
            <button
              className="text-gray-700 outline-none p-2 rounded-md focus:border-gray-400 focus:border"
              onClick={() => setState(!opened)}
            >
              <PiHamburgerDuotone size={42} />
            </button>
          </div>
        </div>
        <div className={`flex-1 justify-self-center pb-3 mt-8 md:block md:pb-0 md:mt-0 ${opened ? "block" : "hidden"}`} >
          <ul className="justify-center items-center space-y-4 md:font-medium md:flex md:space-x-6 md:space-y-0">
            {items.map((item, idx) => (
              <li key={idx}>
                <Link className="block m-0 p-1 border-transparent md:border-b-2 md:m-0 md:p-3 md:px-3 text-gray-700 dark:text-gray-300 dark:hover:text-orange-500 transition-colors hover:text-indigo-800 hover:border-blue-800 dark:hover:border-orange-900" href={item.path}>{item.title}</Link>
              </li>
            ))}
          </ul>
        </div>
        <div className="flex items-center space-x-4">
            {/* language and theme switcher, account popover */}
            <ThemeSwitcher />
          {/* <button className="text-gray-700 outline-none p-2 rounded focus:border-gray-400 focus:border">
            <span className="fi fi-cz rounded-full" />
          </button> */}
        </div>
      </div>
    </nav>
  )
}